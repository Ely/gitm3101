#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
	pid_t pid;

	if(argc != 2) {
		printf("Usage: %s <pid>\n", argv[0]);
	}

	pid = atoi(argv[1]);

	kill(pid, SIGUSR1);
	return 0;
}

