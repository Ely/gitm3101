#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>

void signalHandler(int signal);

typedef void (*PtrFct)(int);

int counts = 0;
jmp_buf ptRep;

int main(int argc, char* argv[]) {
	PtrFct fct;
	int ret;
	char answer[256];

	ret = setjmp(ptRep);

	if(ret == 0) {
		fct = signal(SIGALRM, signalHandler);

		if(fct == SIG_ERR) {
			perror("Erreur signal");
			exit(1);
		}
	}

	printf("Répondez à la question rapidement svp (%d tentatives restantes)\n", (2-counts));
	alarm(2);
	
	fgets(answer, sizeof answer - 1, stdin);

	printf("Votre réponse: %s", answer);

	return 0;
}

void signalHandler(int signal) {
	if(signal != SIGALRM) {
		perror("Erreur signal");
		exit(1);
	}

	counts++;

	if(counts == 2) {
		perror("Vous n'avez pas répondu assez rapidement à la question.\n");
		exit(1);
	}

	sigrelse(SIGALRM);

	longjmp(ptRep, counts);

	exit(0);
}