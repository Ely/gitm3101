#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef void (*PtrFct) (int);

void signalHandler(int signal) {
	if(signal != SIGUSR1) {
		perror("Erreur signal");
		exit(1);
	}
	printf("[%d] Signal %d intercepté !\n", getpid(), signal);
	exit(0);
}

int main(int argc, char* argv[]) {
	PtrFct fct;
	fct = signal(SIGUSR1, (PtrFct) signalHandler);

	if(fct == SIG_ERR) {
		perror("Erreur signal");
		exit(1);
	}

	pause();

	return 0;
}

