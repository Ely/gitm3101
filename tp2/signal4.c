#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 256

typedef void (*PtrFct) (int);

void printError(char *error);

int randomInt(int min, int max);

void signalHandler(int signal);

int main(int argc, char* argv[]) {
	pid_t pid;
	int r, randomTime;
	srand(time(NULL));
	
	if(signal(SIGCHLD, (PtrFct) signalHandler) == SIG_ERR) {
		printError("signal");
	}

	while(1) {
		pid = fork();
		switch(pid) {
		case -1:
			printError("fork");
		case 0:
			srand(time(NULL));
			randomTime = randomInt(3, 8);
			printf("Lancement d'un processus de PID (%d secondes): %d\n", randomTime, getpid());
			sleep(randomTime);
			exit(randomTime);
		default: 
			sleep(randomInt(2, 5));
		}
	}

	return 0;
}

void printError(char *error) {
	char errBuf[BUFFER_SIZE];
	snprintf(errBuf, BUFFER_SIZE, "[-] %s()", error);
	perror(errBuf);
	exit(1);
}

int randomInt(int min, int max) {
	return (rand() % (max - min)) + min;
}

void signalHandler(int signal) {
	pid_t pid;
	int rapport, status;

	if(signal != SIGCHLD) {
		perror("Erreur signal");
	}

	pid = wait(&rapport);

	if(WIFEXITED(rapport)) {
		status = WEXITSTATUS(rapport);
		printf("[%d] Processus enfant terminé en %d secondes.\n", pid, status);
	}
}