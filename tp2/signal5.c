#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define BUFFER_SIZE 64

typedef void (* PtrFct) (int);

void segfaultHandler(int signal);
void floatingPointHandler(int signal);

void printError(char *error);

int main(int argc, char* argv[]) {
	int i, rapport, numSignal, status;
	pid_t pid;

	for(i = 0; i < 3; i++) {
		pid = fork();
		if(pid == 0) break;
	}

	switch(pid) {
		case -1:
		perror("fork()");
		exit(1);
		case 0:
		printf("%d, PID: %d\n", i, getpid());
		int *ptr = NULL;
		switch(i) {
			case 0: // Premier fils
			exit(3);
			break;
			case 1:
			if(signal(SIGSEGV, (PtrFct) segfaultHandler) == SIG_ERR) {
				printError("signal");
			}

			*ptr = 0xdeadbeef;
			break;
			case 2:
			if(signal(SIGFPE, (PtrFct) floatingPointHandler) == SIG_ERR) {
				printError("signal");
			}
			i /= i-2;
			break;
			default:
			break;
		}
	}

	pid = wait(&rapport);
	while(pid != -1) {
		printf("Terminaison du fils de PID : %d\n", pid);
		if(WIFEXITED(rapport)) {
			status = WEXITSTATUS(rapport);
			printf("Terminaison normale du processus de PID %d (%d)\n", pid, status);
		} else {
			if(WIFSIGNALED(rapport)) {
				numSignal = WTERMSIG(rapport);
				printf("Terminaison anormale du processus de PID %d (Signal %d)\n", pid, numSignal);
			} else {
				perror("Erreur système");
			}
		}
		pid = wait(&rapport);
	}

	return 0;
}

void segfaultHandler(int signal) {
	if(signal != SIGSEGV) {
		perror("Erreur signal");
	}
	fprintf(stderr, "Erreur de segmentation sur le processus %d\n", getpid());
	exit(0);
}

void floatingPointHandler(int signal) {
	if(signal != SIGFPE) {
		perror("Erreur signal");
	}
	fprintf(stderr, "Exception de point flottant sur le processus %d\n", getpid());
	exit(0);
}

void printError(char *error) {
	char errBuf[BUFFER_SIZE];
	snprintf(errBuf, BUFFER_SIZE, "[-] %s()", error);
	perror(errBuf);
	exit(1);
}