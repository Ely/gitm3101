#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <time.h>
#include <pthread.h>

#define BUFFER_SIZE 256

typedef void* (*PtrFct(void *));

void printError(char *err);
void displayInformations(void);

void* traitementThread1(void *);
void* traitementThread2(void *);

int main(int argc, char* argv[]) {
	int n = 42;
	int *numPtr;
	char *str = "Hello, World";

	displayInformations();
	pthread_t id1, id2;

	if(pthread_create(&id1, NULL, traitementThread1, (void*)&n) == -1) {
		printError("pthread_create");
	}

	if(pthread_create(&id2, NULL, traitementThread2, (void*)&str) == -1) {
		printError("pthread_create");
	}

	if(pthread_join(id1, (void **)&numPtr) != 0) {
		printError("pthread_join");
	}

	printf("Valeur après thread 1 [%lu] : %d\n", id1, *numPtr);
	free(numPtr);
	numPtr = NULL;

	if(pthread_join(id2, (void **)&str) != 0) {
		printError("pthread_join");
	}

	printf("Chaine après thread 2 [%lu] : %s\n", id2, str);
	free(str);
	str = NULL;

	return 0;
}

void printError(char *err) {
	char errBuf[BUFFER_SIZE];
	snprintf(errBuf, BUFFER_SIZE, "[-] %s()", err);
	perror(errBuf);
	exit(1);
}

void displayInformations(void) {
	time_t t = time(NULL);
	printf("%s %s (%d)\n", ctime(&t), getlogin(), getuid());
}

void* traitementThread1(void *value) {
	pthread_t tid;
	if((tid = pthread_self()) == -1) {
		printError("pthread_self");
	}

	int n = (int) *(int *)value;

	int *ptr = malloc(sizeof(int));
	*ptr = n * 2;
	printf("[%lu] Valeur: %d\n", tid, n);
	pthread_exit((void *)ptr);
}

void* traitementThread2(void *value) {
	pthread_t tid;
	if((tid = pthread_self()) == -1) {
		printError("pthread_self");
	}

	char *str = *(char **)value;
	
	char *buf = malloc(BUFFER_SIZE);
	bzero(buf, BUFFER_SIZE);

	snprintf(buf, BUFFER_SIZE, "Message de Thread2 : %s", str);

	printf("[%lu] %s\n", tid, str);
	pthread_exit((void *)buf);
}