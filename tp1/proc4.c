#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/wait.h>
#include <time.h>

void displayInformations(void);
void describeChild(char* name, int i);
void traitementFils(void);

int main(int argc, char *argv[]) {
	pid_t pid;
	int i, rapport;
	
	displayInformations();
	
	pid = fork();

	if(pid == 0) {
		traitementFils();
	} else {
		wait(&rapport);
	}

	return 0;
}

void describeChild(char* name, int i) {
	printf("%s(%d) - %d\n", name, i, getpid());
}

void displayInformations(void) {
	time_t t = time(NULL);
	printf("%s %s PID: %d\n", ctime(&t), getlogin(), getpid());
}

void traitementFils(void) {
	sleep(2);
	printf("PID: %d, PPID: %d\n", getpid(), getppid());
	exit(0);
}