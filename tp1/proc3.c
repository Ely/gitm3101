#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/wait.h>
#include <time.h>

void displayInformations(void);
void describeChild(char* name, int i);

void traitementFils1(void);
void traitementFils2(void);

int main(int argc, char *argv[]) {
	pid_t pid;
	int i, rapport;

	displayInformations();
	
	for(i = 0; i < 2; i++) {
		pid = fork();
		wait(&rapport);
		if(pid == 0) break;
	}

	if(pid == 0) {
		switch(i) {
			case 0: // Premier processus
			traitementFils1();
			exit(1);
			case 1: // Second processus
			traitementFils2();
			exit(2);
		}
	} else {
		sleep(7);
	}

	return 0;
}

void describeChild(char* name, int i) {
	printf("%s(%d) - %d\n", name, i, getpid());
}

void displayInformations(void) {
	time_t t = time(NULL);
	printf("%s %s PID: %d\n", ctime(&t), getlogin(), getpid());
}

void traitementFils1(void) {
	for(int i = 1; i < 26; i++) printf("%d\n", i);
}

void traitementFils2(void) {
	for(int i = 26; i < 51; i++) printf("%d\n", i);
}