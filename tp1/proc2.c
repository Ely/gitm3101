#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/wait.h>
#include <time.h>

void displayInformations(void);
void describeChild(char* name, int i);

int main(int argc, char *argv[]) {
	pid_t pid;
	int i, rapport, status, numSig;
	char* args[] = {"p2fils2", "42", NULL};

	displayInformations();
	
	for(i = 0; i < 2; i++) {
		pid = fork();
		if(pid == 0) break;
	}

	if(pid == 0) {
		switch(i) {
			case 0: // Premier processus
			execl("./p2fils1", "p2fils1", "Fils 1", NULL);
			case 1: // Second processus
			execvp("./p2fils2", args);
		}
	} else {
		pid = wait(&rapport);
		while(pid != -1) {
			if(WIFEXITED(rapport)) { // Fin normale
				status = WEXITSTATUS(rapport);
				printf("Terminaison normale du processus %d (%d)\n", pid, status);
			} else {
				if(WIFSIGNALED(rapport)) {
					numSig = WTERMSIG(rapport);
					printf("Terminaison anormale du procesus %d (%s)\n", pid, strsignal(numSig));
				}
			}

			pid = wait(&rapport);
		}
	}

	return 0;
}

void describeChild(char* name, int i) {
	printf("%s(%d) - %d\n", name, i, getpid());
}

void displayInformations(void) {
	time_t t = time(NULL);
	printf("%s %s PID: %d\n", ctime(&t), getlogin(), getpid());
}