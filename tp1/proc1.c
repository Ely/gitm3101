#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/wait.h>
#include <time.h>

void displayInformations(void);
void describeChild(char* name, int i);

void traitementFils1(char *name, int i);
void traitementFils2(char *name, int i);
void traitementFils3(char *name, int i);

int main(int argc, char *argv[]) {
	pid_t pid;
	int i, rapport, status, numSig;

	displayInformations();
	
	for(i = 0; i < 3; i++) {
		pid = fork();
		if(pid == 0) break;
	}

	if(pid == 0) {
		switch(i) {
			case 0: // Premier processus
			traitementFils1(argv[0], i);
			case 1: // Second processus
			traitementFils2(argv[0], i);
			case 2: // Troisieme processus
			traitementFils3(argv[0], i);
		}
	} else {
		pid = wait(&rapport);
		while(pid != -1) {
			if(WIFEXITED(rapport)) { // Fin normale
				status = WEXITSTATUS(rapport);
				printf("Terminaison normale du processus %d (%d)\n", pid, status);
			} else {
				if(WIFSIGNALED(rapport)) {
					numSig = WTERMSIG(rapport);
					printf("Terminaison anormale du procesus %d (%s)\n", pid, strsignal(numSig));
				}
			}

			pid = wait(&rapport);
		}
	}

	return 0;
}

void describeChild(char* name, int i) {
	printf("%s(%d) - %d\n", name, i, getpid());
}

void displayInformations(void) {
	time_t t = time(NULL);
	printf("%s %s PID: %d\n", ctime(&t), getlogin(), getpid());
}

void traitementFils1(char *name, int i) {
	describeChild(name, i);
	exit(0);
}

void traitementFils2(char *name, int i) {
	describeChild(name, i);
	*((int*)i) = 0xdeadbeef;
}

void traitementFils3(char *name, int i) {
	describeChild(name, i);
	i /= 0;
}