#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void displayInformations(char *argv[]);

int main(int argc, char* argv[]) {
	if(argc != 2) {
		printf("Usage: %s <string>\n", argv[0]);
		exit(0);
	}

	displayInformations(argv);
	return 0;
}

void displayInformations(char *argv[]) {
	int n = atoi(argv[1]);
	printf("%1$s [%2$d] - %3$d (0x%3$x)\n", argv[0], getpid(), n);
}