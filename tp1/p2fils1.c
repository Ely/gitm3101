#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void displayInformations(char *argv[]);

int main(int argc, char* argv[]) {
	if(argc != 2) {
		printf("Usage: %s <string>\n", argv[0]);
		exit(0);
	}

	displayInformations(argv);
	return 0;
}

void displayInformations(char *argv[]) {
	printf("%s [%d] - %s\n", argv[0], getpid(), argv[1]);
}