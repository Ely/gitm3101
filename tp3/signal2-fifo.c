#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void displayInformations(void);

int main(int argc, char* argv[]) {
	pid_t pid;

	if(argc != 2) {
		printf("Usage: %s <pid>\n", argv[0]);
		exit(1);
	}

	displayInformations();

	pid = atoi(argv[1]);

	printf("[%d] Killing process %d\n", getpid(), pid);

	kill(pid, SIGUSR1);
	return 0;
}

void displayInformations(void) {
	time_t t = time(NULL);
	printf("-----------------------------------\n%s%s [%d]\n", ctime(&t), getlogin(), getpid());
}

