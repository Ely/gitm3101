#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>

#define BUFFER_SIZE 256

void printError(char *error);
void displayInformations(void);

int main(int argc, char* argv[]) {
	pid_t pid;
	int fileDescriptors[2];
	char buffer[BUFFER_SIZE];

	bzero(buffer, BUFFER_SIZE);

	displayInformations();

	if(pipe(fileDescriptors) == -1) {
		perror("[-] pipe()");
		exit(1);
	}

	pid = fork();

	if(pid == -1) {
		perror("[-] perror()");
		exit(1);
	} else if(pid == 0) {
		displayInformations();
		
		close(fileDescriptors[1]);
		FILE *out = fopen("data.txt", "w");
		if(out == NULL) {
			perror("[-] fopen()");
			exit(1);
		}

		while(read(fileDescriptors[0], buffer, sizeof buffer -1)) {
			write(fileno(out), buffer, strlen(buffer));
		}

		exit(0);
	} else {
		close(fileDescriptors[0]);
		while(fgets(buffer, sizeof buffer - 1, stdin) != NULL && strncmp(buffer, "STOP", strlen(buffer) - 1) != 0) {
			write(fileDescriptors[1], buffer, sizeof buffer - 1);
		}
	}

	return 0;
}

void printError(char *error) {
	char errBuf[BUFFER_SIZE];
	snprintf(errBuf, BUFFER_SIZE, "[-] %s()", error);
	perror(errBuf);
	exit(1);
}

void displayInformations(void) {
	time_t t = time(NULL);
	printf("%s %s [%d]\n", ctime(&t), getlogin(), getpid());
}