#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

typedef void (*PtrFct) (int);

void err(char *error);

void displayInformations(void);

void signalHandler(int signal);

int main(int argc, char* argv[]) {
	PtrFct fct;
	int fileDescriptors[2];
	char buf[128];
	pid_t pid;

	fct = signal(SIGUSR1, (PtrFct) signalHandler);

	if(fct == SIG_ERR) {
		err("signal");
	}

	if(pipe(fileDescriptors) == -1) {
		err("pipe");
	}

	pid = fork();

	if(pid == -1) {
		err("fork");
	} else if(pid == 0) {
		displayInformations();

		close(fileDescriptors[1]);

		while(read(fileDescriptors[0], buf, sizeof buf - 1) == 0);

		printf("[%d] Got PID from parent %s\n", getpid(), buf);

		execl("./signal2-fifo", "signal2-fifo", buf, NULL);
	} else {
		displayInformations();

		snprintf(buf, sizeof buf - 1, "%d", getpid());

		printf("[%s] Sending PID to child [%d]\n", buf, pid);

		close(fileDescriptors[0]);

		write(fileDescriptors[1], buf, strlen(buf));

		pause();
	}

	return 0;
}

void err(char *error) {
	char errorBuf[128];
	snprintf(errorBuf, sizeof errorBuf - 1, "[-] %s()", error);
	perror(errorBuf);
	exit(1);
}

void displayInformations(void) {
	time_t t = time(NULL);
	printf("-----------------------------------\n%s %s [%d]\n", ctime(&t), getlogin(), getpid());
}

void signalHandler(int signal) {
	if(signal != SIGUSR1) {
		perror("Erreur signal");
		exit(1);
	}
	printf("[%d] Signal %d intercepté !\n", getpid(), signal);
	exit(0);
}