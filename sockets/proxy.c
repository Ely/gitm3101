#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>

#define BUFFER_SIZE 1024
#define DEFAULT_ADDRESS "127.0.0.1"
#define DEFAULT_PORT 4242

#define REMOTE_ADDRESS "127.0.0.1"
#define REMOTE_PORT 4444

#define QUEUE_LENGTH 5

void printError(char *error);

int main(int argc, char* argv[]) {
	int sock, client, sockTo;
	struct sockaddr_in addr, addrTo;
	socklen_t addressLength;
	char sentMessage[BUFFER_SIZE], receivedMessage[BUFFER_SIZE];
	int writtenCount, readCount;

	if((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		printError("socket");
	} 

	if((sockTo = socket(AF_INET, SOCK_STREAM 0)) == -1) {
		printError("socketTo");
	}

	addressLength = sizeof(struct sockaddr_in);

	memset(&addr, 0, addressLength);

	addr.sin_family = AF_INET;
	addr.sin_port = htons(DEFAULT_PORT);
	inet_aton(DEFAULT_ADDRESS, &addr.sin_addr);

	addrTo.sin_family = AF_INET;
	addrTo.sin_port = htons(REMOTE_PORT);
	inet_aton(REMOTE_ADDRESS, &addrTo.sin_addr);

	if(bind(sock, (struct sockaddr *)&addr, addressLength) == -1) {
		close(sock);
		printError("bind");
	}

	printf("Starting server [%s:%d] \n", DEFAULT_ADDRESS, DEFAULT_PORT);

	if(listen(sock, QUEUE_LENGTH) == -1) {
		close(sock);
		printError("listen");
	}

	printf("Listening...\n");

	while(1) {
		memset(sentMessage, 0, BUFFER_SIZE);
		memset(receivedMessage, 0, BUFFER_SIZE);

		printf("Waiting for a client to connect ...\n");

		if((client = accept(sock, (struct sockaddr *) &addr, &addressLength)) == -1) {
			close(sock);
			printError("accept");
		}

		printf("Client connected !\n");

		readCount = read(client, receivedMessage, BUFFER_SIZE);

		switch(readCount) {
			case -1:
			close(client);
			printError("read");
			exit(1);
			case 0:
			close(client);
			fprintf(stderr, "Socket closed");
			exit(1);
		}

		// Reception d'un message du client [receivedMessage]

		if(connect(sockTo, (struct sockaddr *) &addrTo, addressLength) == -1) {
			close(sockTo);
			printError("connect");
		}

		memset(sentMessage, 0, BUFFER_SIZE);

		strncpy(sentMessage, )

		writtenCount = write(sock, sentMessage, strlen(sentMessage));

		switch(writtenCount) {
			case -1:
			close(sock);
			printError("write");
			case 0:
			fprintf(stderr, "Socket closed");
			exit(1);
			default:
			printf("Message \"%s\" (%d) sent !\n", sentMessage, writtenCount);
		}

		readCount = read(sock, receivedMessage, BUFFER_SIZE);

		switch(readCount) {
			case -1:
			close(sock);
			printError("read");
			case 0:
			fprintf(stderr, "Socket closed");
			exit(1);
			default:
			printf("Message \"%s\" (%d) received !\n", receivedMessage, readCount);
		}

		close(sock);

		snprintf(sentMessage, BUFFER_SIZE, "Message received !");

		writtenCount = write(client, sentMessage, strlen(sentMessage));

		switch(writtenCount) {
			case -1:
			close(client);
			printError("write");
			case 0:
			close(client);
			fprintf(stderr, "Socket closed");
			default:
			printf("Sent message \"%s\" (%d)\n", sentMessage, writtenCount);
		}

		close(client);
	}

	close(sock);

	return 0;
}

void printError(char *error) {
	char buf[BUFFER_SIZE];
	snprintf(buf, BUFFER_SIZE - 1, "[-] %s()", error);
	perror(buf);
	exit(1);
}