#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>

#define BUFFER_SIZE 64
#define DEFAULT_ADDRESS "localhost"
#define DEFAULT_PORT 4242

void printError(char *error);

int main(int argc, char* argv[]) {
	int sock;
	struct sockaddr_in addr;
	socklen_t addressLength;
	int writtenCount, readCount;

	char sentMessage[BUFFER_SIZE];
	char receivedMessage[BUFFER_SIZE];

	if((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		printError("socket");
	}

	addressLength = sizeof addr;

	memset(&addr, 0, addressLength);

	addr.sin_family = AF_INET;
	addr.sin_port = htons(DEFAULT_PORT);
	inet_aton(DEFAULT_ADDRESS, &addr.sin_addr);

	if(connect(sock, (struct sockaddr *) &addr, addressLength) == -1) {
		close(sock);
		printError("connect");
	}

	memset(sentMessage, 0, BUFFER_SIZE);
	memset(receivedMessage, 0, BUFFER_SIZE);

	snprintf(sentMessage, BUFFER_SIZE, "Hello, World");

	writtenCount = write(sock, sentMessage, strlen(sentMessage));

	switch(writtenCount) {
		case -1:
			close(sock);
			printError("write");
		case 0:
			fprintf(stderr, "Socket closed");
			exit(1);
		default:
			printf("Message \"%s\" (%d) sent !\n", sentMessage, writtenCount);
	}

	readCount = read(sock, receivedMessage, BUFFER_SIZE);

	switch(readCount) {
		case -1:
			close(sock);
			printError("read");
		case 0:
			fprintf(stderr, "Socket closed");
			exit(1);
		default:
			printf("Message \"%s\" (%d) received !\n", receivedMessage, readCount);
	}

	close(sock);

	return 0;
}

void printError(char *error) {
	char errBuf[BUFFER_SIZE];
	snprintf(errBuf, BUFFER_SIZE - 1, "[-] %s()", error);
	perror(errBuf);
	exit(1);
}